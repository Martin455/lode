#include "IAdapter.h"

IAdapter::IAdapter(int size): a(size)
{
	int percent = (size*size/100.0) * 30.0;

	part_of_boat = 0;
	hits = 0;
	do
	{
		Boat tmp;
		boats.push_back(tmp);
		part_of_boat += tmp.get_part();
	}
	while(percent > part_of_boat);
}

// void IAdapter::print()
// {
// 	a.debug_print();
// }

void IAdapter::setting_desk()
{
	for (auto x = boats.begin();x!=boats.end();++x)
		a.set_boat(*x);
}

bool IAdapter::try_hit(int x, int y)
{
	int value = a.get_value(x,y);

	if (value==EMPTY)
		return false;
	else
	{
		hits++;
		return true;
	}
}
bool IAdapter::end_game()
{
	if (hits==part_of_boat)
		return true;
	return false;
}

int IAdapter::num_of_boats() const
{
	return boats.size();
}