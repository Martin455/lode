#ifndef BOAT_H
#define BOAT_H

#include <cstdlib>

enum Type_boat {
	ONE,
	TWO,
	THREE,
	FOUR
};

class Boat
{
	Type_boat type;
	int part;
public:
	Boat();
	~Boat() {};

	Type_boat get_type() const;
	int get_part() const;
};

#endif