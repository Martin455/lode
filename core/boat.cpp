#include "boat.h"

Boat::Boat()
{
	double r = rand()/double(RAND_MAX) * 100;
	if (r < 25.0)
	{
		type = ONE;
		part = 1;
	}
	else if (r < 50.0)
	{
		type = TWO;
		part = 2;
	}
	else if (r < 75.0)
	{
		type = THREE;
        part = 4;
	}
	else
	{
		type = FOUR;
		part = 5;
	}

}

Type_boat Boat::get_type() const
{
	return type;
}

int Boat::get_part() const
{
	return part;
}
