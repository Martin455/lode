#include "area.h"

#include <iostream>

Area::Area(int s): size(s)
{
	desk.resize(size);
	for(auto x = desk.begin(); x!= desk.end();++x)
		(*x).resize(size);
}

void Area::debug_print()
{
	for(int i = 0; i < size;++i)
	{
		for(int j = 0; j < size; ++j)
			std::cout << desk[i][j] << " ";
		std::cout << std::endl;
	}
}

int Area::get_value(int x,int y) const
{
	return desk[x][y];
}

void Area::set_boat(Boat &b)
{
	int x = int(rand()/double(RAND_MAX)*size);
	int y = int(rand()/double(RAND_MAX)*size);
	bool cykle = true;
	Type_boat type = b.get_type();
	int num_attempt = 0;

	while(cykle && num_attempt < 20)
	{
		//overeni na pohyb uvnitr pole
		if (x >=size)
			x--;
		if (x < 0)
			x++;
		if (y >= size)
			y--;
		if (y < 0)
			y++;

		switch (type)
		{
			case ONE:
				if (desk[x][y]==BOAT)
				{
					if (x < size)
						x++;
					else if (x > 0)
						x--;
					else if (y < size)
						y++;
					else if (y > 0)
						y--;
					else
						return;
				}
				else
				{
					desk[x][y] = BOAT;
					cykle = false;
				}
				break;
			case TWO:
			{
				int dir = int(rand()/double(RAND_MAX)*4);
				if (dir == 0)	//vyber smeru
				{
					if (desk[x][y]==EMPTY && (x+1)<size && desk[x+1][y]==EMPTY) //zjisteni dostupnosti
					{	
						desk[x][y] = BOAT;
						desk[x+1][y] = BOAT;
						cykle = false;
					}
					else	//pokud neni dostupny posuv
					{
						if ((x+2)<size)
							x++;
					}
				}
				else if (dir == 1)
				{
					if (desk[x][y]==EMPTY && (x-1)>0 && desk[x-1][y]==EMPTY)
					{
						desk[x][y] = BOAT;
						desk[x-1][y] = BOAT;
						cykle = false;
					}
					else
					{
						if ((x-2)>0)
							x--;
					}
				}
				else if (dir == 2)
				{
					if (desk[x][y]==EMPTY && (y+1)<size && desk[x][y+1]==EMPTY)
					{
						desk[x][y] = BOAT;
						desk[x][y+1] = BOAT;
						cykle = false;
					}
					else
					{
						if ((y+2)<size)
							y++;
					}
				}
				else
				{
					if (desk[x][y]==EMPTY && (y-1)>0 && desk[x][y-1]==EMPTY)
					{
						desk[x][y] = BOAT;
						desk[x][y-1] = BOAT;
						cykle = false;
					}
					else
					{
						if ((y-2)>0)
							y--;
					}

				}
				break;
			}
			case THREE:
			{
				int dir = int(rand()/double(RAND_MAX)*4);

				if (dir==0)
				{
					if ((y-2)<0)
						y+=2;
					if ((x+1)>=size)
						x--;
					if ((x-1)<0)
						x++;

					if (desk[x][y]==EMPTY && desk[x][y-1]==EMPTY && desk[x][y-2]==EMPTY && (desk[x+1][y-1]==EMPTY || desk[x-1][y-1]==EMPTY))
					{
						desk[x][y]= BOAT;
						desk[x][y-1]= BOAT;
						desk[x][y-2]= BOAT;

						if (desk[x+1][y-1]==EMPTY)
							desk[x+1][y-1] = BOAT;
						else
							desk[x-1][y-1] = BOAT;
						cykle = false;
					}
					else
					{
						if ((y-3)>0)
							y--;
					}
				}
				else if (dir == 1)
				{
					if ((y+2)>=size)
						y-=2;
					if ((x+1)>=size)
						x--;
					if ((x-1)<0)
						x++;

					if (desk[x][y]==EMPTY && desk[x][y+1]==EMPTY && desk[x][y+2]==EMPTY && (desk[x+1][y+1]==EMPTY || desk[x-1][y+1]==EMPTY))
					{
						desk[x][y]= BOAT;
						desk[x][y+1]= BOAT;
						desk[x][y+2]= BOAT;

						if (desk[x+1][y+1]==EMPTY)
							desk[x+1][y+1] = BOAT;
						else
							desk[x-1][y+1] = BOAT;
						cykle = false;
					}
					else
					{
						if ((y+3)<size)
							y++;
					}

				}
				else if (dir ==2)
				{
					if ((x+2)>=size)
						x-=2;
					if ((y+1)>=size)
						y--;
					if ((y-1)<0)
						y++;

					if (desk[x][y]==EMPTY && desk[x+1][y]==EMPTY && desk[x+2][y]==EMPTY && (desk[x+1][y+1]==EMPTY || desk[x+1][y-1]==EMPTY))
					{
						desk[x][y]= BOAT;
						desk[x+1][y]= BOAT;
						desk[x+2][y]= BOAT;

						if (desk[x+1][y+1]==EMPTY)
							desk[x+1][y+1] = BOAT;
						else
							desk[x+1][y-1] = BOAT;
						cykle = false;
					}
					else
					{
						if ((x+3)<size)
							x++;
					}
				}
				else
				{
					if ((x-2)<0)
						x+=2;
					if ((y+1)>=size)
						y--;
					if ((y-1)<0)
						y++;

					if (desk[x][y]==EMPTY && desk[x-1][y]==EMPTY && desk[x-2][y]==EMPTY && (desk[x-1][y+1]==EMPTY || desk[x-1][y-1]==EMPTY))
					{
						desk[x][y]= BOAT;
						desk[x-1][y]= BOAT;
						desk[x-2][y]= BOAT;

						if (desk[x-1][y+1]==EMPTY)
							desk[x-1][y+1] = BOAT;
						else
							desk[x-1][y-1] = BOAT;
						cykle = false;
					}
					else
					{
						if ((x-3)<size)
							x--;
					}
				}
				break;
			}
			case FOUR:
			{
				if ((x+1)>=size)
					x--;
				if ((x-1)< 0)
					x++;
				if ((y+1)>=size)
					y--;
				if ((y-1)<0)
					y++;

				if (desk[x][y]==EMPTY && desk[x+1][y] ==EMPTY && desk[x-1][y] ==EMPTY && desk[x][y+1] ==EMPTY && desk[x][y-1] ==EMPTY)
				{
					desk[x][y] = BOAT;
					desk[x+1][y] = BOAT;
					desk[x-1][y] = BOAT;
					desk[x][y+1] = BOAT;
					desk[x][y-1] = BOAT;
					cykle = false;
				}
				else
				{
					x = int(rand()/double(RAND_MAX)*size);
					y = int(rand()/double(RAND_MAX)*size);
				}
			}
		}
		num_attempt++;
	}
}
