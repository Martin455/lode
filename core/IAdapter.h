#ifndef IADAPTER_H
#define IADAPTER_H

#include "boat.h"
#include "area.h"

class IAdapter
{
	std::vector<Boat> boats;
	Area a;
	int part_of_boat;
	int hits;
public:
	IAdapter(int size);
    virtual ~IAdapter() {}

	bool try_hit(int x, int y);
	int num_of_boats()const;
	void setting_desk();
	bool end_game();
//	void print(); pro debug
};

#endif
