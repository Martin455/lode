#ifndef AREA_H
#define AREA_H

#include <vector>
#include "boat.h"

const int EMPTY = 0;
const int BOAT = 1;

using std::vector;

class Area
{
	int size;
	vector<vector<int>> desk;

public:
	Area(int s);
	~Area() {};

	int get_value(int x,int y) const;
	void set_boat(Boat &b);

	void debug_print();
};


#endif 