#version 330 core

in vec2 uv;
out vec4 frag_color;

uniform sampler2D tex;
uniform float act;

void main()
{
    frag_color = texture(tex, uv);

    if (act > 0.0)
    {
        if (frag_color.r >= 0.75 && frag_color.g >= 0.75 && frag_color.b >= 0.75)
        {
            frag_color.xyz = vec3(1,0,0);
        }
    }
}
