#include "screen.h"

Screen::~Screen()
{
    if (!window)
        glfwDestroyWindow(window);
}

void Screen::init()
{
    if (!glfwInit())
        return;
    
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(1280, 720, "Lode", nullptr, nullptr);
    if (!window)
        return;
    glfwMakeContextCurrent(window);
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);


    //setting callback function
    glfwSetWindowSizeCallback(window, resize_callback);
    // glfwSetCursorPosCallback(window, mouse_event);

    int width, height;
    glfwGetWindowSize(window, &width, &height);
    glfwSetCursorPos(window, width / 2.0f, height / 2.0f);
}

void Screen::run()
{
    if (!window)
        return;

    opengl_init();

    int ret_value = menu.run();
    if (ret_value == -1)
        return;

    Game *game = new Game(ret_value);
    game->init(window);

    ret_value = game->run();

    //todo nejaky if s retvalue
    delete game;
}

void Screen::opengl_init()
{
    glewExperimental = true;
    if (glewInit() != GLEW_OK)
        return;
    glGetError();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glClearColor(0.2,1.0,0.1,1.0);

    menu.init(window);
}

void Screen::resize_callback(GLFWwindow *window, int width, int height)
{
    (void)window;

    glViewport(0, 0, width, height);
}
