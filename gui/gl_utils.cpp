#include "gl_utils.h"

Shader::~Shader()
{
    glUseProgram(0);
    glDeleteProgram(id);
}
void Shader::bind()
{
    glUseProgram(id);
}
void Shader::unbind()
{
    glUseProgram(0);
}

void Shader::set_mat4(const char *name, const glm::mat4 &matrix)
{
    GLuint location = glGetUniformLocation(id, name);
    glUniformMatrix4fv(location, 1, GL_FALSE, &matrix[0][0]);
}

void Shader::set_vec3(const char *name, const glm::vec3 &vector)
{
    GLuint location = glGetUniformLocation(id, name);
    glUniform3fv(location, 1, &vector[0]);
}

void Shader::set_float(const char *name, float value)
{
    GLuint location = glGetUniformLocation(id, name);
    glUniform1f(location, value);
}

void Shader::init(const char *vertex, const char *fragment)
{
    std::string fragmentCode = get_shader_code(fragment);
    std::string vertexCode = get_shader_code(vertex);
    const char *tmp_vex = vertexCode.c_str();
    const char *tmp_freg = fragmentCode.c_str();

    GLuint vertexshader = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentshader = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(vertexshader,1,&tmp_vex,0);
    glShaderSource(fragmentshader,1,&tmp_freg,0);

    glCompileShader(vertexshader);
    glCompileShader(fragmentshader);

    id = glCreateProgram();
    glAttachShader(id,vertexshader);
    glAttachShader(id,fragmentshader);
    glLinkProgram(id);

    glDeleteShader(vertexshader);
    glDeleteShader(fragmentshader);
}

std::string Shader::get_shader_code(const char *filename)
{
    std::string shader_code("");

    std::ifstream f_stream(filename, std::ios::in);
    if(!f_stream.is_open())
        throw std::runtime_error("Can't open shader file");

    std::string line("");
    while(getline(f_stream,line))
        shader_code += "\n" + line;
    f_stream.close();

    return shader_code;
}
//TEXTURE
Texture::~Texture()
{
    glDeleteTextures(1, &id);
}

void Texture::init(int type, const char *texture_file)
{
    this->type = type;
    int img_width, img_height;
    unsigned char *image = SOIL_load_image(texture_file, &img_width, &img_height, 0, SOIL_LOAD_RGBA);
    if (!image)
        throw std::runtime_error("Failed Texture file");
    
    glGenTextures(1,&id);
    glBindTexture(type,id);
    glTexImage2D(type, 0, GL_RGBA, img_width, img_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    SOIL_free_image_data(image);
    
    glTexParameteri(type,GL_TEXTURE_WRAP_S,GL_REPEAT);
    glTexParameteri(type,GL_TEXTURE_WRAP_T,GL_REPEAT);
    glTexParameteri(type,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(type,GL_TEXTURE_MIN_FILTER,GL_LINEAR);

    glBindTexture(type, 0);
}

void Texture::bind()
{
    glBindTexture(type, id);
}

void Texture::unbind()
{
    glBindTexture(type, 0);   
}

SquareObject::~SquareObject()
{
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);
}

void SquareObject::init()
{
    GLfloat mesh[] = 
    {
        //vert;tex
        -1, 1, 0, 1,
        -1,-1, 0, 0,
         1,-1, 1, 0,
         1, 1, 1, 1,
        -1, 1, 0, 1,
         1,-1, 1, 0
    };

    glGenVertexArrays(1,&vao);
    glBindVertexArray(vao);

    glGenBuffers(1,&vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER,sizeof(mesh),mesh,GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,sizeof(GLfloat)*4,0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(GLfloat)*4,(void*)(sizeof(GLfloat)*2));

    glBindVertexArray(0);
}

void SquareObject::draw()
{
    glBindVertexArray(vao);

    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);  
}
