#ifndef SCENE_H
#define SCENE_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <GLFW/glfw3.h>

class Scene
{
protected:
    GLFWwindow *window;

    virtual void drawing() = 0;
public:
    virtual void init(GLFWwindow *win) = 0;
    virtual int run() = 0;
};

#endif
