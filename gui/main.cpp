#include <ctime>
#include <cstdlib>
#include "screen.h"

int main()
{
    srand(time(nullptr));
    Screen screen;
    screen.init();
    screen.run();

    return 0;
}