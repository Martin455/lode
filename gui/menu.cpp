#include "menu.h"
#include <iostream>

int Menu::active_button = 0;
bool Menu::end_game = false;
bool Menu::new_game = false;
int Menu::area_size = 10;

void Menu::init(GLFWwindow *win)
{
    window = win;
    glfwSetKeyCallback(window, key_event);

    shader.init("./shaders/vertex.glsl", "./shaders/fragment.glsl");
    background.init(GL_TEXTURE_2D, "./textures/background.png");
    object.init();

    button_shader.init("./shaders/button.vert", "./shaders/button.frag");
    new_game_texture.init(GL_TEXTURE_2D, "./textures/newgame_button.png");
    exit_game_texture.init(GL_TEXTURE_2D, "./textures/end_button.png");
    area10_game_texture.init(GL_TEXTURE_2D, "./textures/10x10_button.png");
    area15_game_texture.init(GL_TEXTURE_2D, "./textures/15x15_button.png");
    area20_game_texture.init(GL_TEXTURE_2D, "./textures/20x20_button.png");
}
int Menu::run()
{
    do
    {
        drawing();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    while (glfwWindowShouldClose(window) == 0 && !end_game && !new_game);

    if (end_game && !new_game)
        return -1;
    return area_size;
}

void Menu::active_up()
{
    active_button--;
    if (active_button < 0)
        active_button = 2;
}

void Menu::active_down()
{
    active_button++;
    if (active_button > 2)
        active_button = 0;
}

void Menu::switch_area_size()
{
    if (area_size == 10)
        area_size = 15;
    else if (area_size == 15)
        area_size = 20;
    else
        area_size = 10;
}

void Menu::drawing()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shader.bind();
    background.bind();
    object.draw();
    shader.unbind();
    background.unbind();

    draw_button(0);
    draw_button(1);
    draw_button(2);
}

void Menu::draw_button(int kind)
{
    button_shader.bind();

    button_shader.set_mat4("scale_matrix", glm::scale(glm::mat4(1),glm::vec3(0.2f,0.1f,0.2f)));
    button_shader.set_float("act", 0.0f);
    button_shader.set_float("zpos", -0.1f);
    switch (kind)
    {
        case 0:
            button_shader.set_mat4("translate_matrix", glm::translate(glm::mat4(1),glm::vec3(0.6f,0.0f,0)));
            new_game_texture.bind();
            if (active_button == 0)
                button_shader.set_float("act", 1.0);
            break;
        case 1:
            button_shader.set_mat4("translate_matrix", glm::translate(glm::mat4(1),glm::vec3(0.6f,-0.3f,0)));
            switch (area_size)
            {
                case 10:
                    area10_game_texture.bind();
                    break;
                case 15:
                    area15_game_texture.bind();
                    break;
                case 20:
                    area20_game_texture.bind();
                    break;
            }
            if (active_button == 1)
                button_shader.set_float("act", 1.0);
            break;
        case 2:
            button_shader.set_mat4("translate_matrix", glm::translate(glm::mat4(1),glm::vec3(0.6f,-0.6f,0)));
            exit_game_texture.bind();
            if (active_button == 2)
                button_shader.set_float("act", 1.0);
            break;
    }

    object.draw();
    button_shader.unbind();
    new_game_texture.unbind(); //unbind 2D texture, I don't care which of them
}

void Menu::key_event(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    (void)window;
    (void)scancode;
    (void)mods;
    if (key == GLFW_KEY_UP && action == GLFW_PRESS)
    {
        std::cout << "up" << std::endl;
        active_up();
    }
    if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
    {
        std::cout << "down" << std::endl;
        active_down();
    }
    if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
    {
        std::cout << "enter" << std::endl;
        if (active_button == 2)
            end_game = true;
        else if (active_button == 1)
            switch_area_size();
        else
            new_game = true;
    }
}
