#ifndef SCREEN_H
#define SCREEN_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <GLFW/glfw3.h>
#include "gl_utils.h"
#include "menu.h"
#include "game.h"

class Screen
{
    GLFWwindow *window;

    Menu menu;

    void opengl_init();
public:
    virtual ~Screen();
    void init();
    void run();
    static void resize_callback(GLFWwindow *window, int width, int height);
};

#endif
