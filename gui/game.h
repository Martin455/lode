#ifndef GAME_H
#define GAME_H

#include "../core/IAdapter.h"
#include "scene.h"
#include "gl_utils.h"

struct Coords
{
    int x;
    int y;
    Coords(int _x, int _y)
        : x(_x), y(_y)
    {}
};

class Game: public IAdapter, public Scene
{
    Shader shader;
    Texture background;
    SquareObject object;

    int field_size;
    Texture empty_field;
    Texture bombed_field;
    Shader field_shader;
    std::vector<std::vector<Texture *>> field;

    Texture end_texture;

    void set_hit(int x, int y);
    Coords active_field;

    void draw_field();

protected:
    virtual void drawing();
public:
    Game(int size);
    virtual ~Game() {}

    virtual void init(GLFWwindow *win);
    virtual int run();

    void up_arrow();
    void down_arrow();
    void left_arrow();
    void right_arrow();
    void hit();

    bool empty_fields;
    bool end_game_loop;
    static void key_event(GLFWwindow* window, int key, int scancode, int action, int mods);
};

#endif // GAME_H
