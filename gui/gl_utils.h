#ifndef GL_UTILS_H
#define GL_UTILS_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <SOIL/SOIL.h>
#include <sstream>
#include <fstream>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Shader
{
protected:
    GLuint id;

    std::string get_shader_code(const char *filename);
public:
    virtual ~Shader();
    void init(const char *vertex, const char *fragment);
    void bind();
    void unbind();

    void set_mat4(const char *name, const glm::mat4 &matrix);
    void set_vec3(const char *name, const glm::vec3 &vector);
    void set_float(const char *name, float value);
};

class Texture
{
protected:
    GLuint id;
    int type;

public:
    virtual ~Texture();
    void init(int type, const char *texture_file);
    void bind();
    void unbind();
};

class SquareObject
{
protected:
    GLuint vao;
    GLuint vbo;
public:
    virtual ~SquareObject();
    void init();
    void draw(); //todo transform;
};

#endif
