#ifndef MENU_H
#define MENU_H

#include "scene.h"
#include "gl_utils.h"

class Menu: public Scene
{
    Shader shader;
    Texture background;
    SquareObject object;

    Shader button_shader;
    Texture new_game_texture;
    Texture area10_game_texture;
    Texture area15_game_texture;
    Texture area20_game_texture;
    Texture exit_game_texture;
    
    void draw_button(int kind);

    static int active_button;
    static int area_size;
    static void active_up();
    static void active_down();
    static void switch_area_size();
    static bool end_game;
    static bool new_game;
protected:
    virtual void drawing();
public:
    virtual void init(GLFWwindow *win);
    virtual int run();

    static void key_event(GLFWwindow* window, int key, int scancode, int action, int mods);
};
#endif
