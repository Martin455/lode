#include "game.h"
#include <iostream>

void Game::set_hit(int x, int y)
{
    field[x][y] = &bombed_field;
}

void Game::draw_field()
{
    field_shader.bind();
    field_shader.set_float("zpos", -0.1f);
    glm::vec3 transform_vector;
    float dx;
    float dy;
    float sx;
    if (field_size == 20)
    {
        field_shader.set_mat4("scale_matrix", glm::scale(glm::mat4(1), glm::vec3(0.035f,0.040f,1.0f)));
        transform_vector = glm::vec3(-0.9f, 0.93f, 0.0f);
        sx = -0.9f;
        dx = 0.09f;
        dy = 0.099f;
    }
    else
    {
        field_shader.set_mat4("scale_matrix", glm::scale(glm::mat4(1), glm::vec3(0.038f,0.05f,1.0f)));
        transform_vector = glm::vec3(-0.85f, 0.85f, 0.0f);
        sx = -0.85f;
        dx = 0.11f;
        dy = 0.12f;
    }

    for (int i = 0; i < field_size; ++i)
    {
        for (int j = 0; j < field_size; ++j)
        {
            if (active_field.x == i && active_field.y == j)
            {
                field_shader.set_float("act", 1.0);
            }
            else
            {
                field_shader.set_float("act", 0.0);
            }
            field[i][j]->bind();

            field_shader.set_mat4("translate_matrix", glm::translate(glm::mat4(1),transform_vector));
            object.draw();
            transform_vector.x += dx;
        }
        transform_vector.x = sx;
        transform_vector.y -= dy;
    }
    //unbind 2D texture
    background.unbind();
    field_shader.unbind();
}

void Game::up_arrow()
{
    active_field.x -= 1;
    if (active_field.x < 0)
        active_field.x = 0;
    else if (active_field.x >= field_size)
        active_field.x = field_size - 1;
}

void Game::down_arrow()
{
    active_field.x += 1;
    if (active_field.x < 0)
        active_field.x = 0;
    else if (active_field.x >= field_size)
        active_field.x = field_size - 1;
}

void Game::left_arrow()
{
    active_field.y -= 1;
    if (active_field.y < 0)
        active_field.y = 0;
    else if (active_field.y >= field_size)
        active_field.y = field_size - 1;
}

void Game::right_arrow()
{
    active_field.y += 1;
    if (active_field.y < 0)
        active_field.y = 0;
    else if (active_field.y >= field_size)
        active_field.y = field_size - 1;
}

void Game::hit()
{
    if (try_hit(active_field.x, active_field.y))
        set_hit(active_field.x, active_field.y);
}

void Game::drawing()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shader.bind();
    background.bind();
    object.draw();
    shader.unbind();
    background.unbind();

    draw_field();

    if (end_game())
    {
        field_shader.bind();
        field_shader.set_float("zpos", -0.2f);
        field_shader.set_mat4("scale_matrix", glm::scale(glm::mat4(1),glm::vec3(0.7f,0.7f,0.7f)));
        field_shader.set_mat4("translate_matrix", glm::translate(glm::mat4(1),glm::vec3(0)));
        end_texture.bind();

        object.draw();
        end_texture.unbind();
        field_shader.unbind();

        if (!empty_fields)
            empty_fields = true;
    }
}

Game::Game(int size): IAdapter(size), active_field(0,0)
{
    field_size = size;
}

void Game::init(GLFWwindow *win)
{
    window = win;
    glfwSetKeyCallback(window, key_event);
    //set userpointer to this

    shader.init("./shaders/vertex.glsl", "./shaders/fragment.glsl");
    background.init(GL_TEXTURE_2D, "./textures/background.png");
    object.init();

    end_texture.init(GL_TEXTURE_2D, "./textures/end.png");

    field.resize(field_size);
    for (auto x = field.begin(); x != field.end(); ++x)
        (*x).resize(field_size);

    empty_field.init(GL_TEXTURE_2D, "./textures/empty_field.png");
    bombed_field.init(GL_TEXTURE_2D, "./textures/boat_field.png");
    field_shader.init("./shaders/button.vert", "./shaders/button.frag");

    for (int i = 0; i < field_size; ++i)
        for (int j = 0; j < field_size; ++j)
            field[i][j] = &empty_field;

    glfwSetWindowUserPointer(window, this);

    setting_desk();

    empty_fields = false;
    end_game_loop = false;

    std::cout << num_of_boats() << std::endl;
}

int Game::run()
{
    do
    {
        drawing();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    while(glfwWindowShouldClose(window) == 0 && !end_game_loop);

    return 0;
}

void Game::key_event(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    (void)scancode;
    (void)mods;

    Game *game = static_cast<Game*>(glfwGetWindowUserPointer(window));

    if (key == GLFW_KEY_UP && action == GLFW_PRESS)
    {
        std::cout << "up game" << std::endl;
        game->up_arrow();
    }
    if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
    {
        std::cout << "down game" << std::endl;
        game->down_arrow();
    }
    if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
    {
        std::cout << "right game" << std::endl;
        game->right_arrow();
    }
    if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
    {
        std::cout << "left game" << std::endl;
        game->left_arrow();
    }
    if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
    {
        std::cout << "enter game" << std::endl;

        if (!game->empty_fields)
            game->hit();
    }
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        if (game->empty_fields)
        {
            game->end_game_loop = true;
        }
    }
}
