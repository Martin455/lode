#include <iostream>
#include <ctime>
#include "../core/area.h"
#include "../core/boat.h"
#include "cli_interface.h"

int main(int argc, char **argv)
{
	if (argc < 2)
		return 1;
	char *endptr;
	int size_array = strtol(argv[1],&endptr,10);
	if (*endptr!='\0')
		return 2;

	srand(time(nullptr));
	if (size_array>20) 
		size_array = 20;
	if (size_array<10) 
		size_array = 10;
	
	ICli interface(size_array);
	interface.setting_desk();
	
	interface.exec();

	return 0;
}