#include "cli_interface.h"

ICli::ICli(int s): IAdapter(s)
{
	size_display = s;
	display.resize(size_display);
	for (auto x = display.begin(); x!=display.end();++x)
		(*x).resize(size_display);
	
	for (int i = 0; i < size_display;++i)
		for (int j = 0; j < size_display; ++j)
			display[i][j] = '0';
}

void ICli::exec()
{
	std::cout << "Velikost plochy: " << size_display << std::endl;
	std::cout << "Pocet lodi na zacatku " << num_of_boats() << std::endl; 
	std::cout << "Zadavani od 1 do " << size_display << std::endl;
	do
	{
		print_desk();
		int x,y;
		std::string input;
		std::cout << "Zadej radek: ";
		std::cin >> input;
		x = std::atoi(input.c_str());
		if (x == 0)
		{
			std::cout << "Spatne zadani cele opakuj!" << std::endl;
			continue;
		}
		std::cout << "Zadej sloupec: ";
		std::cin >> input;
		y = std::atoi(input.c_str());
		if (y == 0)
		{
			std::cout << "Spatne zadani cele opakuj!" << std::endl;
			continue;
		}
		x--;
		y--;
		if (try_hit(x,y))
			set_hit(x,y);
	}
	while(!end_game());
}

void ICli::print_desk()
{
	for(int i = 0; i < size_display;++i)
	{
		for(int j = 0; j < size_display; ++j)
			std::cout << display[i][j] << " ";
		std::cout << std::endl;
	}
}

void ICli::set_hit(int x, int y)
{
	display[x][y] = '#';
}