#ifndef CLI_INTERFACE_H
#define CLI_INTERFACE_H

#include "../core/IAdapter.h"
#include <iostream>

class ICli: public IAdapter
{
	int size_display;
	std::vector<std::vector<char>> display;

	void print_desk();
	void set_hit(int x, int y);
public:
	ICli(int s);
    ~ICli() {}

	void exec();
};

#endif
