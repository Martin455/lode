#version 330 core

layout(location=0) in vec2 position;
layout(location=1) in vec2 coords;

out vec2 uv;

uniform mat4 scale_matrix;
uniform mat4 translate_matrix;
uniform float zpos;

void main()
{
    uv = coords;
    gl_Position = translate_matrix * scale_matrix * vec4(position, zpos, 1.0);
}
